<?php
require('../fpdf.php');

class PDF extends FPDF
{
var $B;
var $I;
var $U;
var $HREF;

function PDF($orientation='P', $unit='mm', $size='A4')
{
	// Call parent constructor
	$this->FPDF($orientation,$unit,$size);
	// Initialization
	$this->B = 0;
	$this->I = 0;
	$this->U = 0;
	$this->HREF = '';
}

function WriteHTML($html)
{
	// HTML parser
	$html = str_replace("\n",' ',$html);
	$a = preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
	foreach($a as $i=>$e)
	{
		if($i%2==0)
		{
			// Text
			if($this->HREF)
				$this->PutLink($this->HREF,$e);
			else
				$this->Write(5,$e);
		}
		else
		{
			// Tag
			if($e[0]=='/')
				$this->CloseTag(strtoupper(substr($e,1)));
			else
			{
				// Extract attributes
				$a2 = explode(' ',$e);
				$tag = strtoupper(array_shift($a2));
				$attr = array();
				foreach($a2 as $v)
				{
					if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
						$attr[strtoupper($a3[1])] = $a3[2];
				}
				$this->OpenTag($tag,$attr);
			}
		}
	}
}

function OpenTag($tag, $attr)
{
	// Opening tag
	if($tag=='B' || $tag=='I' || $tag=='U')
		$this->SetStyle($tag,true);
	if($tag=='A')
		$this->HREF = $attr['HREF'];
	if($tag=='BR')
		$this->Ln(5);
}

function CloseTag($tag)
{
	// Closing tag
	if($tag=='B' || $tag=='I' || $tag=='U')
		$this->SetStyle($tag,false);
	if($tag=='A')
		$this->HREF = '';
}

function SetStyle($tag, $enable)
{
	// Modify style and select corresponding font
	$this->$tag += ($enable ? 1 : -1);
	$style = '';
	foreach(array('B', 'I', 'U') as $s)
	{
		if($this->$s>0)
			$style .= $s;
	}
	$this->SetFont('',$style);
}

function PutLink($URL, $txt)
{
	// Put a hyperlink
	$this->SetTextColor(0,0,255);
	$this->SetStyle('U',true);
	$this->Write(5,$txt,$URL);
	$this->SetStyle('U',false);
	$this->SetTextColor(0);
}
}

$html = 'You can now easily print text mixing different styles: <b>bold</b>, <i>italic</i>,
<u>underlined</u>, or <b><i><u>all at once</u></i></b>!<br><br>You can also insert links on
text, such as <a href="http://www.fpdf.org">www.fpdf.org</a>, or on an image: click on the logo.';

$pdf = new PDF('L');
      $pdf->AliasNbPages();
      $pdf->AddPage();
      $linea = 0;
      /*$pdf->SetFillColor(232,232,232);
      $pdf->SetFont('Arial','B',12);
      $pdf->Cell(20,6,'ESTADO',1,0,'C',1);
      $pdf->Cell(70,6,'ID',1,0,'C',1);
      $pdf->Cell(20,6,'MUNICIPIO',1,1,'C',1);
      $pdf->Image('../imagenes/logo.jpg', 50, 50, 30 );*/
      //gg
      
      $pdf->SetTextColor(0, 153, 153);

      $pdf->SetFont('Helvetica', 'B', 30);
      $linea = 20;
      $pdf->Cell(0,$linea,'Oficina de Cultura y Arte UNSA',0,1,'C');

      $pdf->Ln(1);
      //$pdf->Image('../imagenes/logo.jpg', (297-30)/2, $linea+15, 30);
      $pdf->Ln(1);
      $linea = 10;
      $pdf->SetFont('Arial', 'B', 25);
      $pdf->Cell(0,$linea+100,'Alberto Mateos Arellano',0,1,'C');

      $pdf->SetFont('Arial', '', 14);
      $pdf->Cell(0, $linea-90,
       utf8_decode('Con CUI: 20188989 en la fecha 8 de julio del 2018 a superado con distinción'),0,1,'C' );
      $pdf->SetFont('Arial', 'B', 20);
      $pdf->Cell(0, $linea+100, 'Como estudiante', 0,1,'C');
      $pdf->SetFont('Arial', '', 14 );
      $pdf->Cell(0, $linea-90, 'un curso de Guitarra ofrecido por Miguel Caballero',
        0,1,'C' );
      $htmlTable='<TABLE> </TABLE>';
      $pdf->WriteHTML($htmlTable);
$pdf->WriteHTML($html);
$pdf->Output();
?>
